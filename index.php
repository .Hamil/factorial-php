<?php
    /**
     * @author Victor Diaz
     * @email hamil151@hotmail.com
     * @create date 2019-08-21 12:27:41
     * @modify date 2019-08-21 12:27:41
     * @desc [description]
     */
    $num = 6;
    $result = 1;
    for($i = 1; $i <= $num; $i++){
        $result *= $i;
    }
    echo $result;
?>